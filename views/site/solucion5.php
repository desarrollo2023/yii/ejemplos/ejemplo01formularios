<?php

echo \yii\widgets\DetailView::widget([
    "model" => $model,
    "attributes" =>[
      "nombre",
      "apellidos",
      "fecha",
      "email",
      "poblacion",
      "mesTexto"
    ],
]);