<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario7 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio7">

    <?php $form = ActiveForm::begin(); 

        echo $form
                ->field($model, 'fecha')
                ->input('date'); 
        
        echo $form
                ->field($model, 'opciones')
                ->checkboxList($model->opcionesDisponibles()); 
        
        echo $form
                ->field($model, 'identificador')
                ->textInput(["placeholder"=>"Introduce id"]);
                
        echo $form
                ->field($model, 'nombre')
                ->textInput(["placeholder"=>"Introduce el nombre"]); 
        
        echo $form
                ->field($model, 'elementos')
                ->listBox($model->elementosDisponibles(),[
                    "multiple" => "multiple"
                ]); 
        
        echo $form
                ->field($model, 'imagen')
                ->fileInput(); 
        
    ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio7 -->
