<?php

namespace app\models;

use yii\base\Model;

/**
 * Description of Formulario1
 *
 * @author ramon
 */
class Formulario1 extends Model
{
    public ?string $nombre = null; // caja de texto
    public ?string $direccion = null; // area de texto
    public ?int $edad = null; // input tipo numero
    public ?string $fecha = null; // input tipo fecha

    public function attributeLabels(): array
    {
        return [
            "nombre" => "Nombre Completo",
            "direccion" => "Direccion",
            "edad" => "Edad",
            "fecha" => "Fecha Entrada",
        ];
    }

    public function rules()
    {
        return [
            [["nombre", "direccion", "edad", "fecha"], "required"]
        ];
    }
}
